import { UserService } from './user.service';
import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { User } from './schemas/user.schema';
import * as bcrypt from 'bcrypt';
import { RegisterDto } from './dto/register.dto';

@Controller('user')
export class UserController {
  constructor(private readonly UserService: UserService) {}

  @Post('register')
  async createUser(@Body() dto: RegisterDto): Promise<User> {
    const saltOrRounds = 10;
    const password = await bcrypt.hash(dto.password, saltOrRounds);
    const { fullName, address, phoneNumber } = dto;
    const result = await this.UserService.createUser({
      password,
      fullName,
      address,
      phoneNumber,
    });
    return result;
  }

  @Get('users')
  async getUsers(@Query() query: object): Promise<User[]> {
    const users = await this.UserService.getUsers(query);
    return users;
  }
}
