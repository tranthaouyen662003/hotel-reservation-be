import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export enum RoleUser {
  USER = '0',
  ADMIN = '1',
}
export class UserAddress {
  @Prop()
  street: string;

  @Prop()
  ward: string;

  @Prop()
  district: string;
}
@Schema({ timestamps: true })
export class User {
  @Prop()
  username: string;

  @Prop()
  password: string;

  @Prop()
  fullName: string;

  @Prop()
  phoneNumber: string;

  @Prop()
  address: UserAddress;

  @Prop()
  role: RoleUser;
}

export const UserSchema = SchemaFactory.createForClass(User);
