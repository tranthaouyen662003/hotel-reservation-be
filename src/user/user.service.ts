import { User } from './schemas/user.schema';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RegisterDto } from './dto/register.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('user')
    private readonly userModel: Model<User>,
  ) {}

  async createUser(dto: RegisterDto): Promise<User> {
    const { fullName, phoneNumber, address, password } = dto;
    const { street, ward, district } = address;

    const userExists = await this.userModel.exists({ phoneNumber }).exec();
    if (userExists) {
      throw new BadRequestException('PhoneNumber already exists!');
    }

    const newUser = new this.userModel({
      username: phoneNumber,
      password,
      fullName,
      address: {
        street,
        ward,
        district,
      },
    });

    await newUser.save();
    return newUser;
  }

  async getUser(query: object): Promise<User> {
    return this.userModel.findOne(query);
  }

  async getUsers(query: object): Promise<User[]> {
    return this.userModel.find(query);
  }
}
